<?php

require_once __DIR__.'/vendor/autoload.php';

$fio = $_POST['fio'];
$email = $_POST['email'];
$message = $_POST['message'];

$transport = (new Swift_SmtpTransport('ssl://smtp.gmail.com', 465))
    ->setUsername('andreybolonin1989@gmail.com')
    ->setPassword('password');

$mailer = new Swift_Mailer($transport);

$message = (new Swift_Message('Wonderful Subject'))
    ->setFrom(['andreybolonin1989@gmail.com' => 'Andrey Bolonin'])
    ->setTo([$email => $fio])
    ->setBody($message,'text/html');

$total = count($_FILES['upload']['name']);

// Loop through each file
for ($i = 0; $i < $total; $i++) {
    //Get the temp file path
    $tmpFilePath = $_FILES['upload']['tmp_name'][$i];

    //Make sure we have a filepath
    if ($tmpFilePath != "") {
        $newFilePath = "./upload/" . $_FILES['upload']['name'][$i];

        //Upload the file into the temp dir
        if (move_uploaded_file($tmpFilePath, $newFilePath)) {
            $message->attach(Swift_Attachment::fromPath($newFilePath));
        }
    }
}

$result = $mailer->send($message);